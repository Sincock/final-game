using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace finalgame
{

public class EnemyController : MonoBehaviour
{
    public GameObject axis;
    public GameObject Blade;
    public GameObject Player;
    public bool gameOver = false;
    public float speed = 5;
    public float turnSpeed = 10;
        public float enemyHealth = 500;
        public float sawDamage = 15;
        public float spikeDamage = 10;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver == false)
        {
            Blade.transform.RotateAround(axis.transform.position, Vector3.up, -turnSpeed * Time.deltaTime);
        }

        if (gameOver == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, speed * Time.deltaTime);
        }

            void OnCollisionStay(Collision collision)
            {
                if (collision.collider.CompareTag("Saw"))
                {
                    Debug.Log("sawsawsaw");
                    enemyHealth -= sawDamage * Time.deltaTime;
                }
                if (collision.collider.CompareTag("Point"))
                        {
                enemyHealth -= spikeDamage * Time.deltaTime;
            }
                if (enemyHealth == 0)
                {
                    Destroy(this.gameObject);
                }
            }
        }

   
}
}
