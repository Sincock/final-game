using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.VisualScripting;
using DG.Tweening;

namespace finalgame
{

    public class PlayerController : MonoBehaviour
    {
        public float speed = 9;
        public float turnSpeed = 10;
        public float spikeSpeed = 10;
        public float playerHealth = 500;
        public float bladeDamage = 10;
        public GameObject Spike;
        public GameObject TurnPoint;

        // Start is called before the first frame update
        void Start()
        {
           // Spike = GetComponent<GameObject>();
            //TurnPoint= GetComponent<GameObject>();
        }

        IEnumerator Potato()
        {
            Spike.transform.RotateAround(TurnPoint.transform.position, Vector3.right, 90 + spikeSpeed * Time.deltaTime);
            yield return new WaitForSeconds(0.1f *Time.deltaTime);
            Spike.transform.RotateAround(TurnPoint.transform.position, Vector3.left, 90 + spikeSpeed * Time.deltaTime);
            yield return new WaitForSeconds(0.1f * Time.deltaTime);
            Spike.transform.RotateAround(TurnPoint.transform.position, Vector3.forward, 90 + spikeSpeed * Time.deltaTime);
            yield return new WaitForSeconds(0.1f * Time.deltaTime);
            Spike.transform.RotateAround(TurnPoint.transform.position, Vector3.back, 90 + spikeSpeed * Time.deltaTime);
        }

        // Update is called once per frame
        void Update()
        {
            

            if (Input.GetKey(KeyCode.W))
            {
                transform.position += transform.forward * speed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.S))
            {
                transform.position -= transform.forward * speed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.D))
            {
                transform.RotateAround(this.gameObject.transform.position, Vector3.up, turnSpeed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.A))
            {
                transform.RotateAround(this.gameObject.transform.position, Vector3.up, -turnSpeed * Time.deltaTime);
            }

            

            if (Input.GetKey(KeyCode.N))
            {
                //Debug.Log(Spike.transform.localEulerAngles);
               // if (Spike.transform.localEulerAngles.x < 5 )
                 //{
                    StartCoroutine (Potato());
                    
                //}
               // if (Spike.transform.localEulerAngles.x == 80)
               // {
                //    Spike.transform.RotateAround(TurnPoint.transform.position, Vector3.left,45+spikeSpeed * Time.deltaTime);
                // }

            }

           /* if (Spike.transform.localEulerAngles.y > 1)  
            {
                Spike.transform.localEulerAngles = new Vector3(0, 0, 0);
            }

            if (Spike.transform.localEulerAngles.z > 1)
            {
                Spike.transform.localEulerAngles = new Vector3(0,0,0);
            }  

            if (Spike.transform.localEulerAngles.x > 100)
            {
                Spike.transform.localEulerAngles = new Vector3(0,0,0) ;
            }*/
        }
        private void OnCollisionStay(Collision collision)
        {
            if (collision.collider.CompareTag("Blade"))
               {
                Debug.Log("diediedie");
                playerHealth -= bladeDamage * Time.deltaTime;
            }
            if (playerHealth == 0) 
            {
            Destroy(this.gameObject);
            }
        }

    }
}